<div class="sidebar-wrapper">
    <ul class="nav">
        <li class="nav-item active  ">
            <a class="nav-link" href="{{ route('dashboard') }}">
                <i class="material-icons">dashboard</i>
                <p>Dashboard</p>
            </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link" href="{{ route('users.index') }}">
                <i class="material-icons">person</i>
                <p>Users</p>
            </a>
        </li>
    </ul>
</div>
